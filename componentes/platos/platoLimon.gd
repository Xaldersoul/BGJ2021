extends Sprite

var guarnicion = "limon"
var food : String
var bussy = false
signal chequeo_plato

func get_food(foodInn):
	food = foodInn
	$anim.play(foodInn)
	bussy = true
	print("servi limon")
	emit_signal("chequeo_plato",food,guarnicion)
	var t = Timer.new()
	t.set_wait_time(3)
	t.set_one_shot(true)
	self.add_child(t)
	t.start()
	yield(t, "timeout")
	bussy = false
	$anim.play("default")
	t.queue_free()

func get_bussy():
	return bussy
