extends Area2D
var selected = true
var rest_point
var rest_reference
var rest_nodes = []
var bin_nodes = []
var platos_nodes = []
var placed_on : String

var coccion := 4
export var frecuencia = 2
export var timer_path : NodePath
export var food_type : String
onready var timer : Timer = get_node_or_null(timer_path)

func _ready():
	timer.wait_time = frecuencia
	rest_nodes = get_tree().get_nodes_in_group("zones")
	bin_nodes = get_tree().get_nodes_in_group("bin")
	platos_nodes = get_tree().get_nodes_in_group("platos")
	rest_point = self.get_parent().global_position

func init(tag : String):
	placed_on = tag

func _physics_process(delta):
	if selected:
		timer.set_paused(true)
		$anim.visible = true
		global_position = lerp(global_position, get_global_mouse_position(), 25 * delta)
	else:
		timer.set_paused(false)
		global_position = lerp(global_position, rest_point, 10 * delta)
		var distance = global_position.distance_to(rest_point)
		if placed_on == "recipiente" and distance < 0.1:
			self.queue_free()

func _input(event):
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT and not event.pressed:
			selected = false
			var shortest_dist = 110
			for child in bin_nodes: 
				var distance = global_position.distance_to(child.global_position)
				if distance < shortest_dist:
					if rest_reference:
						rest_reference.deselect()
					self.queue_free()
			for child in platos_nodes: 
				var distance = global_position.distance_to(child.global_position)
				if distance < shortest_dist and coccion <= 2 and coccion > 0:
					rest_reference = child
					var bussy = rest_reference.get_bussy()
					if bussy == false:
						rest_reference.get_food(food_type)
						self.queue_free()
			for child in rest_nodes:
				var distance = global_position.distance_to(child.global_position)
				if distance < shortest_dist and child.getBussy() == false:
					timer.start()
					$anim.visible = false
					placed_on = "freidora"
					child.select()
					rest_point = child.global_position
					rest_reference = child
					rest_reference.coccion(coccion)
					shortest_dist = distance

func _on_Timer_timeout():
	coccion -= 1
	rest_reference.coccion(coccion)
	match coccion:
		0:
			timer.stop()
			$anim.play("quemado")
		2:
			$anim.play("cocido")
	print("coccion: " + str(coccion))


func _on_food_input_event(viewport, event, shape_idx):
	if Input.is_action_just_pressed("click"):
		if rest_reference:
			rest_reference.deselect()
		selected = true
