extends Sprite

var clicked : bool
export var food : PackedScene

func _on_Area2D_input_event(viewport, event, shape_idx):
	if Input.is_action_just_pressed("click") and clicked == false :
		clicked = true
		var rabas = food.instance()
		rabas.init("recipiente")
		add_child(rabas)
	yield(get_tree().create_timer(1), "timeout")
	clicked = false
