extends Position2D

var bussy = false

func select():
	$AnimatedSprite.play("crudito")
	bussy = true
	$fritura.play()

func deselect():
	$AnimatedSprite.play("default")
	bussy = false

func getBussy():
	return bussy

func coccion(status):
	match status:
		2:
			$AnimatedSprite.play("punto")
		1:
			$AnimatedSprite.play("punto")
		0:
			$AnimatedSprite.play("quemado")
