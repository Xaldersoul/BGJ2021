extends Node


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var current_track = null
onready var track_list = {"menu":$musica_menu,"carrito":$musica_carrito,"creditos":$creditos}

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func play(_track) :
	
	if current_track == null :
		fade_in(track_list[_track])
	else :
		fade_out(current_track)
		fade_in(track_list[_track])
	
	
func fade_in(_player : AudioStreamPlayer) :
	_player.volume_db = -80
	$Tween.interpolate_property(_player,"volume_db",-80,0,1.5,Tween.TRANS_CIRC,Tween.EASE_OUT)
	$Tween.start()
	_player.play()
	current_track = _player


func fade_out(_player : AudioStreamPlayer) :
	_player.volume_db = 0
	$Tween.interpolate_property(_player,"volume_db",0,-80,1.5,Tween.TRANS_CIRC,Tween.EASE_OUT)
	$Tween.start()
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Tween_tween_completed(object, key):
	if object.volume_db == -80 :
		object.stop()
	pass # Replace with function body.
