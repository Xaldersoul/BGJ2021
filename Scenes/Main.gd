extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	$Cocina.visible = false
	AudioManager.play("carrito")

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Carrito_mostrar_cocina():
	$Cocina.visible = true
	$Carrito.visible = false 


func _on_Cocina_mostrar_carrito():
	$Cocina.visible = false
	$Carrito.visible = true 

