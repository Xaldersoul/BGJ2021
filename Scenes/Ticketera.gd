extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export var platos : Array
signal cliente_atendido

export var carrito_path : NodePath
onready var carrito = get_node_or_null(carrito_path)
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

#Conozca todas las texturas de los tickets que pueda agregar
#Cuando llega la señal de crear ticket, crea un texturerect con el hijo
#Entregar pedido me tendria que decir cual eliminar

func crear_ticket(cliente) :
	var random = randi()%platos.size()
	var ticket = platos[random].instance()
	ticket.cliente = cliente
	$HBoxContainer.add_child(ticket)

func entregar_pedido(comida, guarnicion) :
	print("Llego ticketera")
	var esta : bool = false
	var _ticket 
	for ticket in $HBoxContainer.get_children() :
		if ticket.comparador(comida,guarnicion) :
			_ticket = ticket
			esta = true
			break
	if esta :
			emit_signal("cliente_atendido",_ticket.return_cliente())
			$HBoxContainer.remove_child(_ticket)
			$correcto.play()
			return esta
	else :
		$incorrecto.play()
		print("Error")
		return esta
