extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
signal mostrar_carrito
signal pedido_listo
export var ticketera_path : NodePath
onready var ticketera = get_node_or_null(ticketera_path)

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_TextureButton_pressed():
	emit_signal("mostrar_carrito")


func _on_chequeo_plato(comida, guarnicion):
	print("Un especial")
	emit_signal("pedido_listo",comida,guarnicion)
	pass # Replace with function body.
