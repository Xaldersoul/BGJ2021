extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	AudioManager.play("menu")
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Button_start_pressed():
	#get_tree().change_scene("res://Scenes/Main.tscn")
	$SceneTransition.transition_to("res://Scenes/Main.tscn")


func _on_Button_salir_pressed():
	get_tree().quit()
	


func _on_Button_credits_pressed():
	#get_tree().change_scene("res://Scenes/Credits.tscn")
	$SceneTransition.transition_to("res://Scenes/Credits.tscn")
	
