extends Node2D

#para las skins del cliente
export (Array,Texture) var skins
var indices : Array = []

export var cliente_scene : PackedScene
var client_position = 0
signal mostrar_cocina

export var max_client : int = 5
var cant_client = 0
export var ticketera_path : NodePath
onready var ticketera = get_node_or_null(ticketera_path)


func _ready():
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Timer_timeout():
	cant_client = indices.size()
	if cant_client == max_client : 
		#Esto si queresmos que no dejen de venir clientes.
		pass
	else :
		crear_cliente()

func crear_cliente():
	var client = cliente_scene.instance()
	client.connect("generar_ticket",ticketera,"crear_ticket")
	$YSort.add_child(client)
	randomize()
	var indice = randi()%skins.size()
	while indices.has(indice):
		indice = randi()%skins.size()
	var textura = skins[indice]
	indices.push_back(indice)
	client.normal = textura

func _on_TextureButton_pressed():
	#$ColorRect/AnimationPlayer.play("a_cocina")
	#yield($ColorRect/AnimationPlayer,"animation_finished")
	#print("cambio")
	emit_signal("mostrar_cocina")

func _on_Ticketera_cliente_atendido(_cliente):
	$YSort.remove_child(_cliente)
